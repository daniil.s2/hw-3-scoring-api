#!/usr/bin/env python

import json
import datetime
import logging
import hashlib
import re
import uuid
from collections import namedtuple
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler

from scoring import get_score, get_interests

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class FieldValidationError(Exception):
    def __init__(self, field, cls_field, val, msg):
        super().__init__(
            "Value '{}' of field '{}' doesn't match field type '{}'. {}".format(
                val, field, cls_field, msg
            )
        )


class RequestValidationError(Exception):
    def __init__(self, obj, msg):
        super().__init__("Error validation of {}. {}".format(type(obj), msg))


class Field:
    _undefined = None
    _empty_vals = ('', [], (), {})

    def __init__(self, required=False, nullable=True):
        self.required = required
        self.nullable = nullable
        if self._undefined in self._empty_vals:
            raise ValueError(
                "Empty values list don\'t need contain 'undefined' value."
            )

    def validate(self, field, val):

        if field.startswith('__') and field.endswith('__'):
            raise FieldValidationError(
                field, self.__class__, val,
                "Field has underscore in start or end of name."
            )

        if not self.nullable and val in self._empty_vals:
            raise FieldValidationError(
                field, self.__class__, val,
                "Value must not be empty."
            )

        if self.required and val == self._undefined:
            raise FieldValidationError(
                field, self.__class__, val,
                "Value of field is required."
            )

    def _in_acceptable(self, val):
        # range of possible values ​​for cases when
        # the field value isn't defined or has empty value
        values = self._empty_vals
        values = values if None in values else values + (self._undefined,)
        return val in values


class CharField(Field):
    _empty_vals = ('',)

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        if not isinstance(val, str):
            raise FieldValidationError(
                field, self.__class__, val,
                "Value type must be {}.".format(str)
            )


class ArgumentsField(Field):
    _empty_vals = ({},)

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        if not isinstance(val, dict):
            raise FieldValidationError(
                field, self.__class__, val,
                "Value type must be {}.".format(dict)
            )


class EmailField(CharField):
    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        if '@' not in val:
            raise FieldValidationError(
                field, self.__class__, val,
                "Value must have '@' character."
            )


class PhoneField(Field):
    _empty_vals = ('',)
    _types = (str, int)
    _re_phone = r'7[0-9]{10}'

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        if not self._is_acceptable_type(val):
            types = [str(t) for t in self._types]
            raise FieldValidationError(
                field, self.__class__, val,
                "Value type must be {}.".format(', '.join(types))
            )

        if not re.match(self._re_phone, str(val)):
            raise FieldValidationError(
                field, self.__class__, val,
                "Value doesn't match phone number pattern 7XXXXXXXXXX."
            )

    def _is_acceptable_type(self, val):
        is_valid = any([isinstance(val, t) for t in self._types])
        return is_valid


class DateField(CharField):

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        try:
            datetime.datetime.strptime(val, '%d.%m.%Y')
        except ValueError:
            raise FieldValidationError(
                field, self.__class__, val,
                "Value doesn't match date pattern 'DD.MM.YYYY'"
            )


class BirthDayField(DateField):

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        today = datetime.date.today()
        val = datetime.datetime.strptime(val, '%d.%m.%Y').date()
        diff = today - val
        if diff.days // 365 > 70:
            raise FieldValidationError(
                field, self.__class__, val,
                "Difference between today and birthday must be "
                "no more than 70 years."
            )


class GenderField(Field):
    _empty_vals = ('',)

    choices = (0, 1, 2)

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        if not isinstance(val, int):
            raise FieldValidationError(
                field, self.__class__, val,
                "Value type must be {}.".format(int)
            )

        if val not in self.choices:
            choices = [str(c) for c in self.choices]
            raise FieldValidationError(
                field, self.__class__, val,
                "Value must be {}.".format(', '.join(choices))
            )


class ClientIDsField(Field):
    _empty_vals = ([],)

    def validate(self, field, val):
        super().validate(field, val)

        if self._in_acceptable(val):
            return

        if not isinstance(val, list):
            raise FieldValidationError(
                field, self.__class__, val,
                "Value type must be {}.".format(list)
            )

        is_all_int = all([isinstance(i, int) for i in val])
        if not is_all_int:
            raise FieldValidationError(
                field, self.__class__, val,
                "Type of value elements must be {}.".format(int)
            )


class MetaRequest(type):
    def __new__(meta, name, bases, dct):
        fields = dict()
        verifiable = '_f_verifiable'

        if dct.get(verifiable):
            raise AttributeError(
                "Attribute '{}' is reserved.".format(verifiable)
            )

        for attr, val in dct.items():
            if issubclass(type(val), Field):
                fields[attr] = val

        dct[verifiable] = fields

        return super().__new__(meta, name, bases, dct)


class BaseRequest(metaclass=MetaRequest):
    # 'err_msg' is intended for using outer systems e.g. API response
    err_msg = None

    def __init__(self, **kwargs):

        for field, f_type in self._f_verifiable.items():
            val = kwargs.pop(field, f_type._undefined)
            setattr(self.__class__, field, val)

    def _get_err_fields(self):
        err_fields = list()
        cls_attrs = self.__class__.__dict__

        for field, f_type in self._f_verifiable.items():
            val = cls_attrs.get(field)

            try:
                f_type.validate(field, val)
            except FieldValidationError:
                err_fields.append(field)

        return err_fields

    def validate(self):
        fields = self._get_err_fields()
        if fields:
            self.err_msg = 'Incorrect values for fields: {}.'.format(', '.join(fields))
            raise RequestValidationError(self, self.err_msg)


class ClientsInterestsRequest(BaseRequest):
    client_ids = ClientIDsField(required=True, nullable=False)
    date = DateField(required=False, nullable=True)


class OnlineScoreRequest(BaseRequest):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    _any_no_empty = (
        ('first_name', 'last_name'),
        ('email', 'phone'),
        ('birthday', 'gender')
    )

    def _is_no_empty(self, f_name):
        f_type = self._f_verifiable.get(f_name)
        val = getattr(self, f_name)
        return not f_type._in_acceptable(val)

    def validate(self):
        result = False
        for fields in self._any_no_empty:
            if all([self._is_no_empty(f) for f in fields]):
                result = True
                break

        if not result:
            fields_lists = [str(i) for i in self._any_no_empty]
            self.err_msg = 'All attributes of one of the fields list must be no empty. ' \
                           'Fields lists: {}'.format(', '.join(fields_lists))
            raise RequestValidationError(self, self.err_msg)

        super().validate()


class MethodRequest(BaseRequest):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        data = datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT
        digest = hashlib.sha512(data.encode('UTF-8')).hexdigest()
    else:
        data = request.account + request.login + SALT
        digest = hashlib.sha512(data.encode('UTF-8')).hexdigest()
    logging.debug(digest)
    if digest == request.token:
        return True
    return False


# Methods handlers
# *************************************************************************
# All methods handlers must have parameter '**kwargs'
# so that any handler function can get dictionary like argument, which has any lenght
def online_score_handler(result, store, ctx, method_obj, request_obj, **kwargs):
    """method handler"""
    score = get_score(
        store,
        method_obj.phone,
        method_obj.email,
        method_obj.birthday,
        method_obj.gender,
        method_obj.first_name,
        method_obj.last_name
    )
    score = 42 if request_obj.is_admin else score
    result['score'] = score

    verifiable = method_obj.__class__._f_verifiable
    no_empty_fields = [f for f in verifiable if method_obj._is_no_empty(f)]
    ctx["has"] = no_empty_fields


def clients_interests_handler(result, store, ctx, method_obj, **kwargs):
    """method handler"""
    interests = dict()
    for c_id in method_obj.client_ids:
        interests[c_id] = get_interests(store, c_id)

    result.update(interests)

    ctx["nclients"] = len(method_obj.client_ids)


def method_handler(request, ctx, store):
    response = None
    MethodHandler = namedtuple('MethodHandler', ['type', 'handler'])
    methods_handlers = {
        'online_score': MethodHandler(OnlineScoreRequest, online_score_handler),
        'clients_interests': MethodHandler(ClientsInterestsRequest, clients_interests_handler)
    }

    try:
        body = request.get('body', dict())
        request_obj = MethodRequest(**body)
        request_obj.validate()
    except RequestValidationError:
        code = INVALID_REQUEST
        response = request_obj.err_msg
        return response, code

    if not check_auth(request_obj):
        code = FORBIDDEN
        return response, code

    method = request_obj.method
    if method not in methods_handlers.keys():
        code = INVALID_REQUEST
        return response, code

    m_handler = methods_handlers.get(method)
    try:
        method_obj = m_handler.type(**request_obj.arguments)
        method_obj.validate()
    except RequestValidationError:
        code = INVALID_REQUEST
        response = method_obj.err_msg
        return response, code

    result = dict()
    args = {
        'result': result,
        'store': store,
        'ctx': ctx,
        'method_obj': method_obj,
        'request_obj': request_obj
    }

    m_handler.handler(**args)
    response = result
    code = OK

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logging.info(
                "%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path](
                        {"body": request, "headers": self.headers}, context,
                        self.store)
                except Exception as e:
                    logging.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"),
                 "code": code}
        context.update(r)
        logging.info(context)
        self.wfile.write(json.dumps(r).encode('UTF-8'))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.DEBUG,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logging.info("Starting server at %s" % opts.port)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
